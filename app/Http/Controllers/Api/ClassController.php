<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Model_class\Classes;
use Illuminate\Support\Facades\DB;
class ClassController extends Controller
{
    public function index()
    {
        //
//        $class=DB::table('classes')->get();
//        return response()->json($class); //without modal use
       // return Classes::all();
        $users = Classes::select('id as class_id','name as user_name')->get();
        return response()->json($users);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:classes|max:25',
            'code' => 'required|unique:classes|max:5'
        ]);
        $data=array();
        $data['name']=$request->name;
        $data['code']=$request->code;
        $data['status_active']=1;
        $data['is_delete']=0;
        $data['created_at']=now();
        $result=DB::table('classes')->insertGetId($data);
        return response('Done'.$result);
    }
    public function show(string $id)
    {
        //
        $show=DB::table('classes')->select('name','code','status_active','is_delete','created_at')->where('id',$id)->first();
        return response()->json($show);
    }
    public function edit(string $id)
    {
        //
    }
    public function update(Request $request, string $id)
    {
        //
    }
    public function destroy(string $id)
    {
        //
        DB::table('classes')->where('id',$id)->delete();
        return response('Data is Delete');
    }
}
