<?php
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ClassController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
//Route::apiResource('/class','App\Http\Controllers\Api\ClassController');
//Route::apiResource('/class', ClassController::class)->name('show');
Route::apiResource('class', ClassController::class)->names([
        'index' => 'class.class.index',
        'show' => 'class.class.show',
        'store' => 'class.class.store',
        'update' => 'class.class.update',
        'destroy' => 'class.class.destroy',
    ]);
